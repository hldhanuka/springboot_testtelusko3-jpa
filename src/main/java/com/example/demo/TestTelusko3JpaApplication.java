package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTelusko3JpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestTelusko3JpaApplication.class, args);
	}

}
