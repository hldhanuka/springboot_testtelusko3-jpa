package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dao.AlienRepo;
import com.example.demo.model.Alien;

@Controller
//@RestController
public class AlienController {
	@Autowired
	AlienRepo repo;

	@RequestMapping("/")
	public String home() {
		return "home.jsp";
	}

//	@RequestMapping("/addAlien")
//	public String addAlien(Alien alien) {
//		repo.save(alien);
//
//		return "home.jsp";
//	}

	@RequestMapping("/getAlien")
	public ModelAndView getAlien(@RequestParam int aid) {
		// initialize the model view
		ModelAndView mv = new ModelAndView("showAlien.jsp");

		// get the alien
		Alien alien = repo.findById(aid).orElse(new Alien());

		// get details by tech
		System.err.println(repo.findAllByTech("Java"));

		// get details by aid greater than some value
		System.err.println(repo.findAllByAidGreaterThan(102));

		// get details by tech with order by aname
		System.err.println(repo.findByTechSortedByAname("Java"));

		// set the alien to model view
		mv.addObject(alien);

		return mv;
	}

//	@RequestMapping("/aliens")
//	@ResponseBody
//	public String getAliens() {
//		return repo.findAll().toString();
//	}

	@GetMapping(path="/aliens", produces={"application/json"})
	@ResponseBody
	public List<Alien> getAliens() {
		return repo.findAll();
	}

//	@GetMapping(path="/aliens", produces={"application/xml"})
//	@ResponseBody
//	public List<Alien> getAliens() {
//		return repo.findAll();
//	}

//	@RequestMapping("/alien/{aid}")
//	@ResponseBody
//	public String getAlienApi(@PathVariable("aid") int aid) {
//		return repo.findById(aid).toString();
//	}

	@GetMapping("/alien/{aid}")
//	@ResponseBody
	public Optional<Alien> getAlienApi(@PathVariable("aid") int aid) {
		return repo.findById(aid);
	}
	
	@PostMapping(path="/alien", produces={"application/xml"}, consumes={"application/json"})
	public Alien addAlien(@RequestBody Alien alien) {
		repo.save(alien);

		return alien;
	}

	@DeleteMapping("/alien/{aid}")
	public String deleteAlienApi(@PathVariable("aid") int aid) {
		Alien alien = repo.getOne(aid);

		repo.delete(alien);

		return "deleted";
	}

	@PutMapping(path="/alien", produces={"application/xml"}, consumes={"application/json"})
	public Alien updateAlien(@RequestBody Alien alien) {
		repo.save(alien);

		return alien;
	}
}
