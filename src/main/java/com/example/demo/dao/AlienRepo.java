package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Alien;

public interface AlienRepo extends JpaRepository<Alien, Integer>
{
	List<Alien> findAllByTech(String tech);

	List<Alien> findAllByAidGreaterThan(int aid);

	@Query("from Alien where tech=?1 order by aname")
	List<Alien> findByTechSortedByAname(String tech);
}
